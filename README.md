# Mollie integration for Drupal Commerce 2

* Provides payment method for Drupal Commerce 2 for Drupal 8 (not Drupal Payment)
* @see https://www.drupal.org/project/commerce
* @see https://github.com/mollie/mollie-api-php

# Payment methods that are integrated

* Default: Commerce Checkout

# Installation

* ```$ composer require drupal/commerce_mollie```
* ```$ composer update drupal/commerce_mollie --with-dependencies```
* Enable 'commerce mollie' in your Drupal installation
* Config payment-gateway @ /admin/commerce/config/payment-gateways
* As order-workflow choose 'Default, with validation' @ /admin/commerce/config/order-types/default/edit

# Local development

## Let Mollie callback to your local development machine

You can do this with:

- [ngrok](https://ngrok.com/)
- [localtunnel.me](https://localtunnel.github.io/www/)


## Override api keys in settings.local.php?

```
$config['commerce_payment.commerce_payment_gateway.mollie_payment_gateway']['configuration']['api_key_test'] = 'test key';

$config['commerce_payment.commerce_payment_gateway.mollie_payment_gateway']['configuration']['api_key_live'] = 'live key';

$config['commerce_payment.commerce_payment_gateway.mollie_payment_gateway']['configuration']['callback_domain'] = 'http://example.com';
```
